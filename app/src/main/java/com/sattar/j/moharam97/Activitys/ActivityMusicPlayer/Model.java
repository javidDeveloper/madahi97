package com.sattar.j.moharam97.Activitys.ActivityMusicPlayer;

import com.sattar.j.moharam97.Contracts.MusicPlayerActivityContract;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;
import com.sattar.j.moharam97.Models.Retrofit.GetRest;

import java.util.List;

public class Model implements MusicPlayerActivityContract.Model {
    private MusicPlayerActivityContract.Presenter presenter;
    private GetRest rest = new GetRest();

    //
    public Model() {
        rest.attachmusicPlayerAcModel(this);
    }


    @Override
    public void attachPresenter(MusicPlayerActivityContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getItems(int musicPage, String musicCat) {
        rest.getMusicMusicPlayer(musicPage, musicCat);
    }

    @Override
    public void addview(int id) {
        rest.addView(id);
    }

    @Override
    public void onListSuccessItems(List<MusicPojo> music, String cat) {
        presenter.onListSuccessItems(music, cat);
    }

    @Override
    public void onListFailedItems(String msg) {
        presenter.onListFailedItems(msg);
    }


}
