package com.sattar.j.moharam97.Models.Pojo;

import android.support.v7.widget.RecyclerView;

/**
 * Created by javid on 10/12/18.
 */

public class RecyclerViewParentModel {
    private RecyclerView header;
    private RecyclerView cat1;
    private RecyclerView cat2;
    private RecyclerView cat3;

    public RecyclerView getHeader() {
        return header;
    }

    public void setHeader(RecyclerView header) {
        this.header = header;
    }

    public RecyclerView getCat1() {
        return cat1;
    }

    public void setCat1(RecyclerView cat1) {
        this.cat1 = cat1;
    }

    public RecyclerView getCat2() {
        return cat2;
    }

    public void setCat2(RecyclerView cat2) {
        this.cat2 = cat2;
    }

    public RecyclerView getCat3() {
        return cat3;
    }

    public void setCat3(RecyclerView cat3) {
        this.cat3 = cat3;
    }
}
