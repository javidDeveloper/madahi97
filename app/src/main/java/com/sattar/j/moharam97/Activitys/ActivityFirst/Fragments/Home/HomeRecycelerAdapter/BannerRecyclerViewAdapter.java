package com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home.HomeRecycelerAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sattar.j.moharam97.Models.Pojo.BannerPojo;
import com.sattar.j.moharam97.MyWidgets.MyImageView;
import com.sattar.j.moharam97.R;

import java.util.List;

/**
 * Created by javid on 9/29/18.
 */

public class BannerRecyclerViewAdapter extends RecyclerView.Adapter<BannerRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private List<BannerPojo> pojos;
    private View rootView;

    public BannerRecyclerViewAdapter(Context context, List<BannerPojo> pojos) {
        this.context = context;
        this.pojos = pojos;
    }

    public void setMusicPojos(List<BannerPojo> pojos) {
        this.pojos = pojos;
    }

    @Override
    public BannerRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        rootView = LayoutInflater.from(context).inflate(R.layout.layout_banner_item, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull BannerRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.avatar.Load(pojos.get(position).getLink());
    }

    @Override
    public int getItemCount() {
        return pojos != null ? pojos.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        MyImageView avatar;

        public ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.imgAvatar);
        }
    }
}
