package com.sattar.j.moharam97.Managers;

import com.orm.query.Condition;
import com.orm.query.Select;
import com.sattar.j.moharam97.Models.ORMDatabase.MusicPojoDB;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;

import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {
    public static DatabaseManager databaseManager;
    private List<MusicPojoDB> musicPojosDb = new ArrayList<>();
    private List<MusicPojo> musicPojos = new ArrayList<>();
    private MusicPojoDB db;
    private MusicPojo musicPojo;

    public static DatabaseManager getInstance() {
        if (databaseManager == null) {
            databaseManager = new DatabaseManager();
        }
        return databaseManager;
    }

    public List<MusicPojoDB> getMusicPojosDb() {
        return musicPojosDb;
    }

    public void setMusicPojosDb(List<MusicPojoDB> musicPojosDb) {
        this.musicPojosDb = musicPojosDb;
    }

    public DatabaseManager() {

    }

    public void saveFavListMusic(List<MusicPojo> musicPojos) {

        for (MusicPojo musicPojo : musicPojos) {
            if (MusicPojoDB.count(MusicPojoDB.class, "id_web = ?", new String[]{musicPojo.getId()}) >= 1) {
                musicPojosDb = Select.from(MusicPojoDB.class).where(Condition.prop("id_web").eq(musicPojo.getId())).list();
                for (int i = 0; i <= musicPojosDb.size(); i++) {
                    db = musicPojosDb.get(i);
                    db.setAddBy(musicPojo.getAddBy());
                    db.setCate(musicPojo.getCate());
                    db.setCat(musicPojo.getCat());
                    db.setIdWeb(musicPojo.getId());
                    db.setIdUnic(musicPojo.getIdUnic());
                    db.setTitle(musicPojo.getTitle());
                    db.setText_music(musicPojo.getText_music());
                    db.setLink(musicPojo.getLink());
                    db.setLikePost(musicPojo.getLikePost());
                    db.setAvatar(musicPojo.getAvatar());
                    db.setArtist(musicPojo.getArtist());
                    db.setTextMusic(musicPojo.getText_music());
                    db.setUserFav(1);
                    db.save();
                }
            } else {
                db = new MusicPojoDB();
                db.setAddBy(musicPojo.getAddBy());
                db.setCate(musicPojo.getCate());
                db.setCat(musicPojo.getCat());
                db.setIdWeb(musicPojo.getId());
                db.setIdUnic(musicPojo.getIdUnic());
                db.setTitle(musicPojo.getTitle());
                db.setText_music(musicPojo.getText_music());
                db.setLink(musicPojo.getLink());
                db.setLikePost(musicPojo.getLikePost());
                db.setDate(System.currentTimeMillis() + "");
                db.setAvatar(musicPojo.getAvatar());
                db.setArtist(musicPojo.getArtist());
                db.setTextMusic(musicPojo.getText_music());
                db.setUserFav(1);
                db.save();
            }
        }

    }

    public void saveFavMusic(MusicPojo musicPojo, boolean fav) {

        if (MusicPojoDB.count(MusicPojoDB.class, "id_web = ?", new String[]{musicPojo.getId()}) >= 1) {
            musicPojosDb = Select.from(MusicPojoDB.class).where(Condition.prop("id_web").eq(musicPojo.getId())).list();
            for (MusicPojoDB db : musicPojosDb) {
                if (!fav) {
                    db.delete();
                }
            }
        } else {
            db = new MusicPojoDB();
            db.setAddBy(musicPojo.getAddBy());
            db.setCate(musicPojo.getCate());
            db.setCat(musicPojo.getCat());
            db.setIdWeb(musicPojo.getId());
            db.setIdUnic(musicPojo.getIdUnic());
            db.setTitle(musicPojo.getTitle());
            db.setText_music(musicPojo.getText_music());
            db.setLink(musicPojo.getLink());
            db.setLikePost(musicPojo.getLikePost());
            db.setDate(System.currentTimeMillis() + "");
            db.setAvatar(musicPojo.getAvatar());
            db.setArtist(musicPojo.getArtist());
            db.setTextMusic(musicPojo.getText_music());
            db.setUserFav(fav ? 1 : 0);
            db.save();
        }
    }

    public List<MusicPojoDB> readListMusicFromDB() {
        db = new MusicPojoDB();
        db = new MusicPojoDB();
        musicPojosDb = Select.from(MusicPojoDB.class).orderBy("date").list();
        return musicPojosDb;
    }

    public int readFavMusicDB(MusicPojo pojo) {
        musicPojosDb = Select.from(MusicPojoDB.class).where(Condition.prop("id_web").eq(pojo.getId())).and(Condition.prop("user_fav").eq(1)).list();
        if (musicPojosDb.size() != 0) {
            return 1;
        } else {
            return 0;
        }
    }
}
