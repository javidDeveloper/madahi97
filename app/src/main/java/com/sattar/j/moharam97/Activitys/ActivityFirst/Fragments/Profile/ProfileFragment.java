package com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Profile;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sattar.j.moharam97.R;

/**
 * Created by javid on 11/15/18.
 */

public class ProfileFragment extends Fragment {
    private View view;
    public static ProfileFragment fragment;

    public static ProfileFragment newInstance() {
        if (fragment == null)
            fragment = new ProfileFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        return view;
    }
}