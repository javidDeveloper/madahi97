package com.sattar.j.moharam97.MyWidgets;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.request.RequestOptions;

import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class MyImageView extends AppCompatImageView {
    public MyImageView(Context context) {
        super(context);
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    public void Load(String url){
        Glide.with(this).load(url).into(this);
    }
    public void LoadBlure(String url,int transform){
        Glide.with(this)
                .load(url)
                .apply(bitmapTransform(new BlurTransformation(transform)))
                .into(this);

    }
    public void LoadCircle(String url){
        Glide.with(this)
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .into(this);

    }
}
