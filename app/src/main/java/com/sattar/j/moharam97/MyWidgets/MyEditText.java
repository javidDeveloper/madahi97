package com.sattar.j.moharam97.MyWidgets;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class MyEditText extends AppCompatEditText {
    private Typeface face;

    public MyEditText(Context context) {
        super(context);
        typeYekan();
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        typeYekan();
    }


    public void typeYekan(){
        face = Typeface.createFromAsset(getContext().getAssets(),"font/Shabnam.ttf");
        setTypeface(face);
    }
}
