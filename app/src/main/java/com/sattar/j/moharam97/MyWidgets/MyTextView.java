package com.sattar.j.moharam97.MyWidgets;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class MyTextView  extends AppCompatTextView {
    private  Typeface face;
    public MyTextView(Context context) {
        super(context);
        typeFont();
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        typeFont();
    }

    public MyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        typeFont();
    }

    public void typeFont(){
        face = Typeface.DEFAULT;
        if(face.isBold()){
            face = Typeface.createFromAsset(getContext().getAssets(),"font/Shabnam-Bold.ttf");
            setTypeface(face);
        }else if(face.isItalic()){
            face = Typeface.createFromAsset(getContext().getAssets(),"font/Shabnam-Light.ttf");
            setTypeface(face);
        }else{
            face = Typeface.createFromAsset(getContext().getAssets(),"font/Shabnam.ttf");
            setTypeface(face);
        }
    }
}
