package com.sattar.j.moharam97.Publics;

import android.app.Activity;
import android.app.Application;
import android.content.Context;



/**
 * Created by javid on 10/31/18.
 */

public class ContextModel extends Application {

    private static Context context;
    private static Activity currentActivity;

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        ContextModel.context = context;
    }

    public static Activity getCurrentActivity() {
        return currentActivity;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        ContextModel.currentActivity = currentActivity;
    }
}