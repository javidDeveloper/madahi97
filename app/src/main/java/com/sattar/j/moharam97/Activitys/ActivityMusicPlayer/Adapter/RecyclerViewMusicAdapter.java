package com.sattar.j.moharam97.Activitys.ActivityMusicPlayer.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.balysv.materialripple.MaterialRippleLayout;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;
import com.sattar.j.moharam97.Activitys.ActivityMusicPlayer.MusicPlayerActivity;
import com.sattar.j.moharam97.MyWidgets.MyImageView;
import com.sattar.j.moharam97.MyWidgets.MyTextView;
import com.sattar.j.moharam97.R;
import com.sattar.j.moharam97.Services.MusicPlayer;
import com.sattar.j.moharam97.Tools.OnItemClickListener;

import java.io.File;
import java.util.List;

/**
 * Created by javid on 9/29/18.
 */

public class RecyclerViewMusicAdapter extends RecyclerView.Adapter<RecyclerViewMusicAdapter.ViewHolder> {
    private Context context;
    private List<MusicPojo> musicPojos;
    private View rootView;
    private MusicPlayerActivity activity = null;
    private OnItemClickListener clickListener;


    public void setActivity(MusicPlayerActivity activity) {
        this.activity = activity;
    }

    public OnItemClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public RecyclerViewMusicAdapter(Context context, List<MusicPojo> musicPojos) {
        this.context = context;
        this.musicPojos = musicPojos;
    }

    public void setMusicPojos(List<MusicPojo> musicPojos) {
        this.musicPojos = musicPojos;
    }

    @Override
    public RecyclerViewMusicAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        rootView = LayoutInflater.from(context).inflate(R.layout.layout_music_player_list_item, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewMusicAdapter.ViewHolder holder, final int position) {
        holder.avatar.LoadCircle(String.valueOf(new File(context.getExternalFilesDir("avatars"),musicPojos.get(position).getId()+".png")));
        holder.artist.setText(musicPojos.get(position).getArtist());
        holder.title.setText(musicPojos.get(position).getTitle());
        holder.itemMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                activity.getMusic(musicPojos.get(position), ADAPTER_MUSIC);
                Intent intent = new Intent(context, MusicPlayer.class);
                intent.putExtra(MusicPlayerActivity.PLAY_MUSIC, musicPojos.get(position));
                context.startService(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (musicPojos.size() < 10) {
            return musicPojos != null ? musicPojos.size() : 0;
        } else
            return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        MyImageView avatar;
        MyTextView title;
        MyTextView artist;
        LinearLayout itemMusic;

        public ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.avatarImg);
            title = itemView.findViewById(R.id.titleMusicTxt);
            artist = itemView.findViewById(R.id.artistNameTxt);
            itemMusic = itemView.findViewById(R.id.bodyClick);
        }
    }
}
