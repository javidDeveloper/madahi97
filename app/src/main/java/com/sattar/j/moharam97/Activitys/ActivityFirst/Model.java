package com.sattar.j.moharam97.Activitys.ActivityFirst;

import com.sattar.j.moharam97.Contracts.FirstActivityContract;
import com.sattar.j.moharam97.Models.Pojo.BannerPojo;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;
import com.sattar.j.moharam97.Models.Retrofit.GetRest;

import java.util.List;

public class Model implements FirstActivityContract.Model {
    private FirstActivityContract.Presenter presenter;
    private GetRest rest = new GetRest();

    public Model() {
        rest.attachfirstAcModel(this);
    }

    @Override
    public void attachPresenter(FirstActivityContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getItems(int musicPage, String musicCat) {
        rest.getListMusic(musicPage, musicCat);
    }

    @Override
    public void getHeaders(int bannerLimit) {
        rest.getBanner(bannerLimit);
    }

    @Override
    public void onListSuccessItems(List<BannerPojo> bannerPojos, List<MusicPojo> music, String cat) {
        presenter.onListSuccessItems(bannerPojos, music, cat);
    }


    @Override
    public void onListFailedItems(String msg) {
        presenter.onListFailedItems(msg);
    }


}
