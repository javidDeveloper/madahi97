package com.sattar.j.moharam97.Activitys.ActivityMusicPlayer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;
import com.sattar.j.moharam97.Contracts.DownloadManagerStatus;
import com.sattar.j.moharam97.Contracts.MusicPlayerActivityContract;
import com.sattar.j.moharam97.Managers.DatabaseManager;
import com.sattar.j.moharam97.Managers.DownloadManager;
import com.sattar.j.moharam97.Models.ORMDatabase.MusicPojoDB;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;
import com.sattar.j.moharam97.Activitys.ActivityMusicPlayer.Adapter.RecyclerViewMusicAdapter;
import com.sattar.j.moharam97.MyWidgets.MyButton;
import com.sattar.j.moharam97.MyWidgets.MyImageView;
import com.sattar.j.moharam97.MyWidgets.MyTextView;
import com.sattar.j.moharam97.Publics.BaseActivity;
import com.sattar.j.moharam97.Publics.ContextModel;
import com.sattar.j.moharam97.R;
import com.sattar.j.moharam97.Services.MusicPlayer;
import com.sattar.j.moharam97.Tools.Utility;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import co.mobiwise.library.MusicPlayerView;


public class MusicPlayerActivity extends BaseActivity implements MusicPlayerActivityContract.View,
        View.OnClickListener, DownloadManagerStatus.Requester {
    public static final String PLAY_MUSIC = "play.music";
    private MusicPojo musicPojo;
    private CoordinatorLayout coordinator;
    private List<MusicPojo> musicPojos = new ArrayList<>();
    private LinearLayout download_320;
    private LinearLayout download_128;
    private LinearLayout download_ogg;
    private RecyclerViewMusicAdapter adapter;
    private Presenter presenter;
    private MyTextView artistMusicTxt;
    private MyTextView titleMusicTxt;
    private TextView countViewMusicTxt;
    private TextView countLikeMusicTxt;
    private MyTextView textMusicTxt;
    private MyTextView moreTxt;
    private MyImageView musicBackground;
    private MyImageView imageFav;
    private MyImageView imageBack;
    private RecyclerView recMusicPlayer;
    private String cat;
    private MusicPlayerView ipv;
    private Intent intent;
    private MediaPlayer player;
    private FloatingActionButton fab;
    private FABToolbarLayout fabToolbar;
    private PopupWindow popupWindowDownload;
    private TextView nameMusic;
    private MyTextView textViewProgress;
    private MyButton buttonDownload;
    private MyButton buttonCancel;
    private ProgressBar progressBar;
    private MyImageView imageViewAvatarDialog;

    public MediaPlayer getPlayer() {
        return player;
    }

    public void setPlayer(MediaPlayer player) {
        this.player = player;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public void setMusicPojo(MusicPojo musicPojo) {
        this.musicPojo = musicPojo;
    }

    public MusicPojo getMusicPojo() {
        return musicPojo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_player);
        getIntentMusic();
        presenter = new Presenter();
        presenter.attachView(this);
        ContextModel.setContext(mContext);
        ContextModel.setCurrentActivity(this);
        DownloadManager.getInstance().attachRequester(this);
        presenter.addview(Integer.parseInt(getMusicPojo().getId()));
        presenter.getItems(1, "new");
        initView();
        setsTexts();
        innerRecycler();
        initFab();
        if (DatabaseManager.getInstance().readFavMusicDB(getMusicPojo()) == 1) {
            imageFav.setBackgroundResource(R.drawable.ic_fav);
        } else {
            imageFav.setBackgroundResource(R.drawable.ic_un_fav);
        }
        imageFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DatabaseManager.getInstance().readFavMusicDB(getMusicPojo()) == 1) {
                    imageFav.setBackgroundResource(R.drawable.ic_un_fav);
                    DatabaseManager.getInstance().saveFavMusic(getMusicPojo(), false);
                } else if (DatabaseManager.getInstance().readFavMusicDB(getMusicPojo()) == 0) {
                    imageFav.setBackgroundResource(R.drawable.ic_fav);
                    DatabaseManager.getInstance().saveFavMusic(getMusicPojo(), true);
                }
            }
        });
    }

    private void initFab() {
        download_320.setOnClickListener(this);
        download_128.setOnClickListener(this);
        download_ogg.setOnClickListener(this);
        fab.setOnClickListener(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void timer(MediaPlayer player) {
        this.player = player;
        mPlayMusic(getPlayer());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void mPojo(MusicPojo pojo) {
        setMusicPojo(pojo);
        setsTexts();
        mPlayMusic(getPlayer());
    }

    private void setsTexts() {
        if (getMusicPojo().getText_music() == null) {
            textMusicTxt.setText("متن موجود نیست");
        } else
            textMusicTxt.setText(Utility.setMusicFullText(getMusicPojo().getText_music()));
        moreTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabToolbar.hide();
                if (getMusicPojo().getText_music() == null) {
                    textMusicTxt.setText("متن موجود نیست");
                } else {
                    if (textMusicTxt.getText().equals("")) {
                        textMusicTxt.setText(Utility.setMusicFullText(getMusicPojo().getText_music()));
                    }
                    if (textMusicTxt.getText().length() <= 55 && textMusicTxt.getText().length() >= 50) {
                        textMusicTxt.setText(getMusicPojo().getText_music());
                    } else {
                        textMusicTxt.setText(Utility.setMusicFullText(getMusicPojo().getText_music()));
                    }
                }
            }
        });
        countViewMusicTxt.setText(getMusicPojo().getViewPost());
        countLikeMusicTxt.setText(getMusicPojo().getLikePost());
        artistMusicTxt.setText(getMusicPojo().getArtist());
        titleMusicTxt.setText(getMusicPojo().getTitle());
    }

    private void mPlayMusic(MediaPlayer secondMusic) {
        long duration = secondMusic.getDuration();
        long seconds = TimeUnit.MILLISECONDS.toSeconds(duration);
        intent = new Intent(mContext, MusicPlayer.class);
        try {
            musicBackground.LoadBlure(String.valueOf(new File(mContext.getExternalFilesDir("avatars"), musicPojo.getId() + ".png")), 55);
            ipv.setCoverURL(getMusicPojo().getAvatar());
            if (!ipv.isRotating()) {
                ipv.toggle();
            }
            ipv.setProgress(secondMusic.getCurrentPosition());
            ipv.setMax((int) seconds);
            ipv.start();
            ipv.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    fabToolbar.hide();
                    if (ipv.isRotating()) {
                        ipv.stop();
                        intent.putExtra(MusicPlayer.STATUS_MUSIC, "pause");
                    } else {
                        intent.putExtra(MusicPlayer.STATUS_MUSIC, "play");
                        ipv.start();
                    }
                    startService(intent);
                }
            });

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ipv.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                    @Override
                    public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                    }
                });
            }
        } catch (Exception e) {
            Log.e("mPlayMusic: ", "Music Activity : mPlayMusic");
        }
    }

    private void innerRecycler() {
        adapter = new RecyclerViewMusicAdapter(mContext, musicPojos);
        adapter.setActivity(this);
        recMusicPlayer.setNestedScrollingEnabled(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayout.VERTICAL, false);
        recMusicPlayer.setLayoutManager(layoutManager);
        recMusicPlayer.setAdapter(adapter);

    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }


    private void initView() {
        artistMusicTxt = findViewById(R.id.artistMusicTxt);
        coordinator = findViewById(R.id.coordinator);
        fab = findViewById(R.id.fabtoolbar_fab);
        fabToolbar = findViewById(R.id.fabtoolbar);
        titleMusicTxt = findViewById(R.id.titleMusicTxt);
        countViewMusicTxt = findViewById(R.id.countViewMusicTxt);
        countLikeMusicTxt = findViewById(R.id.countLikeMusicTxt);
        recMusicPlayer = findViewById(R.id.recMusicPlayer);
        musicBackground = findViewById(R.id.musicBackground);
        imageFav = findViewById(R.id.imageView_fav);
        imageBack = findViewById(R.id.back);
        textMusicTxt = findViewById(R.id.textMusicTxt);
        moreTxt = findViewById(R.id.moreTxt);
        download_128 = findViewById(R.id.download_320);
        download_320 = findViewById(R.id.download_128);
        download_ogg = findViewById(R.id.download_ogg);
        ipv = findViewById(R.id.ipv);
    }

    private void getIntentMusic() {
        try {
            Serializable pojo = getIntent().getSerializableExtra("track");
            setMusicPojo((MusicPojo) pojo);
            intent = new Intent(this, MusicPlayer.class);
            intent.putExtra(PLAY_MUSIC, getMusicPojo());
            startService(intent);
        } catch (Exception e) {
            Toast.makeText(this, "MusicPlayerActivity getIntentMusic() ", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onListSuccessItems(List<MusicPojo> music, String cat) {
        File file = new File(String.valueOf(mContext.getExternalFilesDir("avatars")));
        for (MusicPojo musicPojo : music) {
            DownloadManager.getInstance().Download(musicPojo.getAvatar(), musicPojo.getId() + ".png", String.valueOf(file), 1);
            musicPojos.add(musicPojo);

        }
        adapter.setMusicPojos(musicPojos);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void musicPojoonListFailedItems(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View view) {
//        int id = view.getId();
        switch (view.getId()) {
            case R.id.fabtoolbar_fab:
                fabToolbar.show();
                break;
            case R.id.download_320:
                fabToolbar.hide();
                showPopUpDownload(getMusicPojo());
                break;
            case R.id.download_128:
                fabToolbar.hide();
                showPopUpDownload(getMusicPojo());
                break;
            case R.id.download_ogg:
                fabToolbar.hide();
                showPopUpDownload(getMusicPojo());
                break;
        }
    }

    private void showPopUpDownload(final MusicPojo musicPojo) {

        try {
            LayoutInflater inflater = (LayoutInflater)
                    getSystemService(LAYOUT_INFLATER_SERVICE);
            final View popupView = inflater != null ? inflater.inflate(R.layout.dialog_downloader, null) : null;
            int width = LinearLayout.LayoutParams.WRAP_CONTENT;
            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
            boolean focusable = false;
            nameMusic = popupView.findViewById(R.id.textViewMusicName);
            textViewProgress = popupView.findViewById(R.id.textViewProgress);
            imageViewAvatarDialog = popupView.findViewById(R.id.imageView_avatar_dialog);
            buttonDownload = popupView.findViewById(R.id.buttonDownload);
            buttonCancel = popupView.findViewById(R.id.buttonCancel);
            progressBar = popupView.findViewById(R.id.progressBar);
            imageViewAvatarDialog.Load(String.valueOf(new File(mContext.getExternalFilesDir("avatars"), musicPojo.getId() + ".png")));

            nameMusic.setText(musicPojo != null ? musicPojo.getTitle() : "");
            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DownloadManager.getInstance().cancelDl(1);
                    popupWindowDownload.dismiss();
                }
            });
            buttonDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getPermissionDownload()) return;
                    String path = Environment.getExternalStorageDirectory().getPath() + "/" + "Music";
                    DownloadManager.getInstance().DownloadMusic(musicPojo.getLink(), musicPojo.getTitle() + ".mp3", path, 1);
                    buttonDownload.setEnabled(false);
                    buttonCancel.setEnabled(true);
                }
            });

            popupWindowDownload = new
                    PopupWindow(popupView, width, height, focusable);
            popupWindowDownload.setOutsideTouchable(true);
            popupWindowDownload.setAnimationStyle(Fade.IN);
            popupWindowDownload.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean getPermissionDownload() {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1100);
            return true;
        }
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1200);
            return true;
        }
        return false;
    }


    @Override
    public void onSuccessDl(String success, int id) {
        popupWindowDownload.dismiss();
        Toast.makeText(mContext, R.string.downloadSuccessfullay, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailDl(String error, int id) {
        popupWindowDownload.dismiss();
        Toast.makeText(mContext, R.string.downloadFailed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressbarDl(int progressPercent, int id) {
        progressBar.setProgress((int) progressPercent);
        progressBar.setIndeterminate(false);

    }

    @Override
    public void showTextViewProgressDl(String progressPercent, int id) {
        textViewProgress.setText(progressPercent);
    }


    @Override
    public void onBackPressed() {
        fabToolbar.hide();
        super.onBackPressed();
    }
}
