package com.sattar.j.moharam97.Activitys.ActivityMusicPlayer.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sattar.j.moharam97.Activitys.ActivityMusicPlayer.BottomSheet;
import com.sattar.j.moharam97.MyWidgets.MyImageView;
import com.sattar.j.moharam97.MyWidgets.MyTextView;
import com.sattar.j.moharam97.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by javid on 11/12/18.
 */

public class BottomSheetAdapter extends BaseAdapter {

    Context mContext;

    LayoutInflater mLayoutInflater = null;

    ArrayList<BottomSheet> mBottomSheets;

    public BottomSheetAdapter(Context context, ArrayList<BottomSheet> bottomSheets) {
        mContext = context;
        mBottomSheets = bottomSheets;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mBottomSheets.size();
    }

    @Override
    public Object getItem(int position) {
        return mBottomSheets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mBottomSheets.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.layout_music_player_fb_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        BottomSheet sheet = (BottomSheet) getItem(position);
        Picasso.with(mContext)
                .load(sheet.getBottomSheetMenuType().getResId())
                .into(viewHolder.mMenuIcon);
        viewHolder.mMenuTitle.setText(sheet.getBottomSheetMenuType().getName());
        return convertView;
    }

    static class ViewHolder {

        MyImageView mMenuIcon;
        MyTextView mMenuTitle;

        public ViewHolder(View view) {
            mMenuIcon = view.findViewById(R.id.mMenuIcon);
            mMenuTitle = view.findViewById(R.id.mMenuTitle);
        }
    }
}