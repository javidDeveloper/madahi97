package com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sattar.j.moharam97.Contracts.FirstActivityContract;
import com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Home.HomeRecycelerAdapter.ParentRecyclerViewAdapter;
import com.sattar.j.moharam97.Activitys.ActivityFirst.Presenter;
import com.sattar.j.moharam97.Managers.DatabaseManager;
import com.sattar.j.moharam97.Managers.DownloadManager;
import com.sattar.j.moharam97.Models.Pojo.BannerPojo;
import com.sattar.j.moharam97.Models.Pojo.MusicPojo;
import com.sattar.j.moharam97.Models.Pojo.RecyclerViewParentModel;
import com.sattar.j.moharam97.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by javid on 10/25/18.
 */

public class CateAllFragment extends android.support.v4.app.Fragment implements FirstActivityContract.View {

    public static final String CATE_NEW = "new";
    public static final String CATE_VIEW = "view";
    public static final String CATE_LIKE = "like";
    private RecyclerView parentRecycler;
    private RecyclerViewParentModel parentModel;
    private List<RecyclerView> parentModels = new ArrayList<>();
    ;
    private ParentRecyclerViewAdapter adapter;
    private View rootView;
    private Presenter presenter;

    public static CateAllFragment fragment;

    public static CateAllFragment newInstance() {
        if (fragment == null)
            fragment = new CateAllFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestLists();
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        requestLists();
    }

    private void requestLists() {
        presenter = new Presenter();
        presenter.attachView(this);
        presenter.getHeaders(15);
        presenter.getItems(1, CATE_NEW);
        presenter.getItems(1, CATE_VIEW);
        presenter.getItems(1, CATE_LIKE);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new Presenter();
        presenter.attachView(this);
        presenter.getHeaders(15);
        presenter.getItems(1, CATE_NEW);
        presenter.getItems(1, CATE_VIEW);
        presenter.getItems(1, CATE_LIKE);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recycler_all_home, container, false);
        parentRecycler = rootView.findViewById(R.id.parentRecycler);
        setTools();
        return rootView;
    }

    private void setTools() {
        parentRecycler.setNestedScrollingEnabled(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        parentRecycler.setLayoutManager(layoutManager);
        parentRecycler.setItemAnimator(new DefaultItemAnimator());
        parentModel = new RecyclerViewParentModel();
        parentModels.add(parentModel.getHeader());
        parentModels.add(parentModel.getCat1());
        parentModels.add(parentModel.getCat2());
        parentModels.add(parentModel.getCat3());
        adapter = new ParentRecyclerViewAdapter(getActivity(), parentModels);

    }

    @Override
    public void onListSuccessItems(List<BannerPojo> bannerPojos, List<MusicPojo> music, String cat) {
        File file = new File(String.valueOf(getContext().getExternalFilesDir("avatars")));
        for (MusicPojo musicPojo : music) {
            DownloadManager.getInstance().Download(musicPojo.getAvatar(), musicPojo.getId() + ".png", String.valueOf(file), 1);
        }
        adapter.setPojoMusicList(music);
        adapter.setPojoHeader(bannerPojos);
        adapter.notifyDataSetChanged();
        parentRecycler.setAdapter(adapter);

    }

    @Override
    public void onListFailedItems(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();

    }
}

