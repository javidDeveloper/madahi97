package com.sattar.j.moharam97.Models.Pojo;

/**
 * Created by javid on 10/25/18.
 */

public class TimerMediaPlayer {
    private long duration;
    private String min;
    private String sec;

    public TimerMediaPlayer() {
    }

    public TimerMediaPlayer(long duration) {
        this.duration = duration;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getSec() {
        return sec;
    }

    public void setSec(String sec) {
        this.sec = sec;
    }
}
