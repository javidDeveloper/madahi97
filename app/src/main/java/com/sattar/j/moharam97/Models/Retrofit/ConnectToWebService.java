package com.sattar.j.moharam97.Models.Retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConnectToWebService {

    private static String Domain_Web;
    private static final String BASE_URL = "http://javiddeveloper.ir/";
//    private static final String TEST_URL = "http://www.mocky.io/v2/";
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}

