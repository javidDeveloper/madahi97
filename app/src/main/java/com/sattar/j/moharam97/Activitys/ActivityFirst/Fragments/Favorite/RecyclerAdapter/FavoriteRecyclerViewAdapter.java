package com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Favorite.RecyclerAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sattar.j.moharam97.Activitys.ActivityMusicPlayer.MusicPlayerActivity;
import com.sattar.j.moharam97.Models.ORMDatabase.MusicPojoDB;
import com.sattar.j.moharam97.MyWidgets.MyImageView;
import com.sattar.j.moharam97.MyWidgets.MyTextView;
import com.sattar.j.moharam97.R;

import java.io.File;
import java.util.List;

/**
 * Created by javid on 9/29/18.
 */

public class FavoriteRecyclerViewAdapter extends RecyclerView.Adapter<FavoriteRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private List<MusicPojoDB> musicPojos;
    private View rootView;

    public FavoriteRecyclerViewAdapter(Context context, List<MusicPojoDB> musicPojos) {
        this.context = context;
        this.musicPojos = musicPojos;
    }

    public void setMusicPojos(List<MusicPojoDB> musicPojos) {
        this.musicPojos = musicPojos;
    }

    @Override
    public FavoriteRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        rootView = LayoutInflater.from(context).inflate(R.layout.layout_music_list_big_item, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteRecyclerViewAdapter.ViewHolder holder, final int position) {
        holder.avatar.Load(String.valueOf(new File(context.getExternalFilesDir("avatars"),musicPojos.get(position).getId()+".png")));
        holder.artist.setText(musicPojos.get(position).getArtist());
        holder.title.setText(musicPojos.get(position).getTitle());
        holder.itemMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MusicPlayerActivity.class);
                intent.putExtra("track", String.valueOf(musicPojos.get(position)));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return musicPojos != null ? musicPojos.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        MyImageView avatar;
        MyTextView title;
        MyTextView artist;
        CardView itemMusic;

        public ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.imgAvatar);
            title = itemView.findViewById(R.id.txtTitleMusic);
            artist = itemView.findViewById(R.id.txtArtistName);
            itemMusic = itemView.findViewById(R.id.cardItemMusic);
        }
    }
}
