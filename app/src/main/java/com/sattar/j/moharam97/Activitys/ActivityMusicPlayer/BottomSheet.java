package com.sattar.j.moharam97.Activitys.ActivityMusicPlayer;

import com.sattar.j.moharam97.R;

/**
 * Created by javid on 11/12/18.
 */

public class BottomSheet {

    public enum BottomSheetMenuType {
        EMAIL(R.drawable.ic_camera, "Mail"), ACCOUNT(R.drawable.ic_camera,
                "Acount"), SETTING(R.drawable.ic_camera, "Setitng");

        int resId;

        String name;

        BottomSheetMenuType(int resId, String name) {
            this.resId = resId;
            this.name = name;
        }

        public int getResId() {
            return resId;
        }

        public String getName() {
            return name;
        }
    }

    BottomSheetMenuType bottomSheetMenuType;

    public static BottomSheet to() {
        return new BottomSheet();
    }

    public BottomSheetMenuType getBottomSheetMenuType() {
        return bottomSheetMenuType;
    }

    public BottomSheet setBottomSheetMenuType(BottomSheetMenuType bottomSheetMenuType) {
        this.bottomSheetMenuType = bottomSheetMenuType;
        return this;
    }
}