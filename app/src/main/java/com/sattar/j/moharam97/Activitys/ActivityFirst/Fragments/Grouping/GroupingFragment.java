package com.sattar.j.moharam97.Activitys.ActivityFirst.Fragments.Grouping;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sattar.j.moharam97.R;

/**
 * Created by javid on 11/15/18.
 */

public class GroupingFragment extends Fragment {
    private View view;
    public static GroupingFragment fragment;

    public static GroupingFragment newInstance() {
        if (fragment == null)
            fragment = new GroupingFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_goruping, container, false);
        return view;
    }
}